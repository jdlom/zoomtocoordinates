# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ZoomToCoordinates
                                 A QGIS plugin
 Zoom,Pan and Highlight Entered Coordinates
                              -------------------
        begin                : 2013-04-10
        copyright            : (C) 2013 by Vinayan Parameswaran
        email                : vinayan123@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import *
from qgis.core import *
from qgis.gui import *
# Initialize Qt resources from file resources.py
#from . import resources_rc
# Import the code for the dialog
from .zoomtocoordinatesdialog import ZoomToCoordinatesDialog
from qgis.utils import iface
import os


class ZoomToCoordinates:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        self.canvas = iface.mapCanvas()
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'TestPlugin_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = ZoomToCoordinatesDialog()
        # add icons
        self.dlg.ui.mActionZoomTo.setIcon(QIcon(
            os.path.join(self.plugin_dir, 'icons', 'zoom.png')))
        self.dlg.ui.mActionPan.setIcon(QIcon(
            os.path.join(self.plugin_dir,'icons', 'pan.png')))
        self.dlg.ui.mActionFlash.setIcon(QIcon(
            os.path.join(self.plugin_dir,'icons', 'flash.png')))

        self.dlg.setWindowModality(QtCore.Qt.NonModal)
        self.dlg.setParent(self.iface.mainWindow(),QtCore.Qt.Dialog)
        #self.dlg = QtGui.QMainWindow(self.iface.mainWindow(), QtCore.Qt.Dialog)
        # add projection selection 
        self.projection_selection_widget = QgsProjectionSelectionWidget()
        self.projection_selection_widget.resize(400, 30)
        self.projection_selection_widget.setCrs(QgsCoordinateReferenceSystem('EPSG:4326'))
        self.dlg.ui.toolBar.addWidget(self.projection_selection_widget)
        #add Spinbox to toolbar 
        lblScale = QLabel("Scale View By")
        self.dlg.ui.toolBar.addSeparator() 
        self.dlg.ui.toolBar.addWidget(lblScale)    
        spBox = QSpinBox()
        self.dlg.ui.toolBar.addWidget(spBox)

        self.spinBox = spBox
        
        self.dlg.ui.toolBar.setFixedHeight(30)
        #validations
        loc = QLocale(QLocale.C) #. as decimal instead of comma
        validator = QtGui.QDoubleValidator()
        validator.setLocale(loc)
        lEditX = self.dlg.ui.mTxtX
        lEditY = self.dlg.ui.mTxtY       
        lEditX.setValidator(validator)
        lEditY.setValidator(validator)

        
        
        #create rubberband for point..for qgis 1.9 and higher
        self.rubberBand = None
        
        #add rubberbands for cross
        self.crossRb = QgsRubberBand(iface.mapCanvas(),QgsWkbTypes.LineGeometry)
        self.crossRb.setColor(Qt.black)

        self.mapcanvas = iface.mapCanvas()
        self.crs = self.mapcanvas.mapSettings().destinationCrs()
        self.mapcanvas.destinationCrsChanged.connect(self.updateCrs)

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(os.path.join(self.plugin_dir, 'icons',"icon.png")),
            u"ZoomToCoordinates", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)        
        self.dlg.ui.mActionZoomTo.triggered.connect(self.zoom)
        self.dlg.ui.mActionPan.triggered.connect(self.pan)
        self.dlg.ui.mActionFlash.triggered.connect(self.flash)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&ZoomToCoordinates", self.action)
        
        #configure rubberbands..
        
        self.rubberBand = QgsRubberBand(self.canvas,QgsWkbTypes.PointGeometry)
        self.rubberBand.setColor(Qt.red)
        #self.rubberBand.setIcon(QgsRubberBand.IconType.ICON_CIRCLE)
        self.rubberBand.setIconSize(7)
        

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&ZoomToCoordinates", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = 1
        #result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
            pass
            
    def zoom(self):
        print("zoom button clicked!")
        x, y = self.get_point()
        
        if not x:
            return
        
        if not y:
            return
            
        scale = self.spinBox.value()
        print("scale is - " + str(scale))
        rect = QgsRectangle(float(x)-scale,float(y)-scale,float(x)+scale,float(y)+scale)
        self.canvas.setExtent(rect)
        pt = QgsPointXY(float(x),float(y))
        self.highlight(pt)
        self.canvas.refresh()
        
    def pan(self):
        print("pan button clicked!")
        x, y = self.get_point()
        
        if not x:
            return
        
        if not y:
            return
        
        
        canvas = self.canvas
        currExt = canvas.extent()
        
        canvasCenter = currExt.center()
        dx = float(x) - canvasCenter.x()
        dy = float(y) - canvasCenter.y()
        
        xMin = currExt.xMinimum() + dx
        xMax = currExt.xMaximum() + dx
        yMin = currExt.yMinimum() + dy
        yMax = currExt.yMaximum() + dy
        
        newRect = QgsRectangle(xMin,yMin,xMax,yMax)
        canvas.setExtent(newRect)
        pt = QgsPointXY(float(x),float(y))
        self.highlight(pt)
        canvas.refresh()
        
    def flash(self):
        print("flash button clicked!")

        x, y = self.get_point()

        if not x:
            return
        
        if not y:
            return
        pt = QgsPointXY(float(x),float(y))
        self.highlight(pt)
            

    def get_point(self):
        x = self.dlg.ui.mTxtX.text()
        y = self.dlg.ui.mTxtY.text()
        
        if not x:
            return None, None
        
        if not y:
            return None, None

        point = QgsPointXY(float(x), float(y))
        try:
            transform = QgsCoordinateTransform(
                self.projection_selection_widget.crs(),
                self.crs, QgsProject.instance())
            point = transform.transform(point)
        except QgsCsException:
                iface.messageBar().pushMessage("Error", "Transformation impossible. You should check the parameters (crs or the coordinates)")
                return None, None
        return point.x(), point.y()

    def highlight(self,point):
        print("highlighting..")
        canvas = self.canvas
        
        currExt = canvas.extent()
        
        leftPt = QgsPoint(currExt.xMinimum(),point.y())
        rightPt = QgsPoint(currExt.xMaximum(),point.y())
        
        topPt = QgsPoint(point.x(),currExt.yMaximum())
        bottomPt = QgsPoint(point.x(),currExt.yMinimum())
        
        horizLine = QgsGeometry.fromPolyline( [ leftPt , rightPt ] )
        vertLine = QgsGeometry.fromPolyline( [ topPt , bottomPt ] )
        
        self.crossRb.reset(QgsWkbTypes.LineGeometry)
        self.crossRb.addGeometry(horizLine,None)
        self.crossRb.addGeometry(vertLine,None)
        
        rb = self.rubberBand
        rb.reset(QgsWkbTypes.PointGeometry)
        rb.addPoint(point)
            
        # wait .5 seconds to simulate a flashing effect
        QTimer.singleShot(500,self.resetRubberbands)
    
    def resetRubberbands(self):
        print("resetting rubberbands..")
        canvas = self.canvas
        
        self.rubberBand.reset()
        
        self.crossRb.reset()
        print("completed resetting..")

    def updateCrs(self):
        self.crs = self.mapcanvas.mapSettings().destinationCrs()
        
