# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ZoomToCoordinatesDialog
                                 A QGIS plugin
 Zoom,Pan and Highlight Entered Coordinates
                             -------------------
        begin                : 2013-04-10
        copyright            : (C) 2013 by Vinayan Parameswaran
        email                : vinayan123@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt5 import QtCore, QtWidgets
from .ui_zoomtocoordinates import Ui_ZoomToCoordinates
# create the dialog for zoom to point


class ZoomToCoordinatesDialog(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(ZoomToCoordinatesDialog,self).__init__(parent)
        #QtWidgets.QDialog.__init__(self)
        # Set up the user interface from Designer.
        self.ui = Ui_ZoomToCoordinates()      
      
        self.ui.setupUi(self)
