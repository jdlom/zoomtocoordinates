# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_zoomtocoordinates.ui'
#
# Created: Wed Apr 10 23:53:28 2013
#      by: PyQt5 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import uic
import os
from qgis.gui import QgsProjectionSelectionWidget

try:
    from qgis.gui import QgsProjectionSelectionWidget
except ImportError:
    pass

from qgis.core import QgsCoordinateReferenceSystem

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

import sys
sys.path.append(os.path.dirname(__file__))

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'ui_zoomtocoordinates.ui'), resource_suffix='')

class Ui_ZoomToCoordinates(QMainWindow, FORM_CLASS):
    def __init__(self, parent=None):
        super(Ui_ZoomToCoordinates, self).__init__(parent)
        self.setupUi(self)
        #add projectionSelectionWidget()
        
        #self.horizontalLayout.addWidget(self.projection_selection_widget)
        # self.toolBar.addAction(self.mActionZoomTo)
        # self.toolBar.addAction(self.mActionPan)
        # self.toolBar.addAction(self.mActionFlash)
        #self.toolBar.addWidget(self.projection_selection_widget)


# class Ui_ZoomToCoordinates(object):
#     def setupUi(self, ZoomToCoordinates):
#         ZoomToCoordinates.setObjectName(_fromUtf8("ZoomToCoordinates"))
#         ZoomToCoordinates.setWindowModality(QtCore.Qt.WindowModal)
#         ZoomToCoordinates.resize(338, 71)
#         sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
#         sizePolicy.setHorizontalStretch(0)
#         sizePolicy.setVerticalStretch(0)
#         sizePolicy.setHeightForWidth(ZoomToCoordinates.sizePolicy().hasHeightForWidth())
#         ZoomToCoordinates.setSizePolicy(sizePolicy)
#         ZoomToCoordinates.setMinimumSize(QtCore.QSize(338, 71))
#         ZoomToCoordinates.setMaximumSize(QtCore.QSize(338, 71))
#         ZoomToCoordinates.setIconSize(QtCore.QSize(16, 16))
#         ZoomToCoordinates.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
#         ZoomToCoordinates.setTabShape(QtGui.QTabWidget.Rounded)
#         self.centralwidget = QtGui.QWidget(ZoomToCoordinates)
#         self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
#         self.layoutWidget = QtGui.QWidget(self.centralwidget)
#         self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 310, 29))
#         self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
#         self.horizontalLayout = QtGui.QHBoxLayout(self.layoutWidget)
#         self.horizontalLayout.setMargin(0)
#         self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
#         self.label = QtGui.QLabel(self.layoutWidget)
#         font = QtGui.QFont()
#         font.setPointSize(10)
#         self.label.setFont(font)
#         self.label.setObjectName(_fromUtf8("label"))
#         self.horizontalLayout.addWidget(self.label)
#         self.mTxtX = QtGui.QLineEdit(self.layoutWidget)
#         self.mTxtX.setObjectName(_fromUtf8("mTxtX"))
#         self.horizontalLayout.addWidget(self.mTxtX)
#         self.label_2 = QtGui.QLabel(self.layoutWidget)
#         font = QtGui.QFont()
#         font.setPointSize(10)
#         self.label_2.setFont(font)
#         self.label_2.setObjectName(_fromUtf8("label_2"))
#         self.horizontalLayout.addWidget(self.label_2)
#         self.mTxtY = QtGui.QLineEdit(self.layoutWidget)
#         self.mTxtY.setObjectName(_fromUtf8("mTxtY"))
#         self.horizontalLayout.addWidget(self.mTxtY)
#         ZoomToCoordinates.setCentralWidget(self.centralwidget)
#         self.toolBar = QtGui.QToolBar(ZoomToCoordinates)
#         self.toolBar.setMovable(False)
#         self.toolBar.setObjectName(_fromUtf8("toolBar"))
#         ZoomToCoordinates.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
#         self.mActionZoomTo = QtGui.QAction(ZoomToCoordinates)
#         icon = QtGui.QIcon()
#         icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/icons/zoom.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
#         self.mActionZoomTo.setIcon(icon)
#         self.mActionZoomTo.setObjectName(_fromUtf8("mActionZoomTo"))
#         self.mActionPan = QtGui.QAction(ZoomToCoordinates)
#         icon1 = QtGui.QIcon()
#         icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/icons/pan.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
#         self.mActionPan.setIcon(icon1)
#         self.mActionPan.setObjectName(_fromUtf8("mActionPan"))
#         self.mActionFlash = QtGui.QAction(ZoomToCoordinates)
#         icon2 = QtGui.QIcon()
#         icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/icons/icons/flash.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
#         self.mActionFlash.setIcon(icon2)
#         self.mActionFlash.setObjectName(_fromUtf8("mActionFlash"))

#         #add projectionSelectionWidget()
#         self.projection_selection_widget = QgsProjectionSelectionWidget()
#         self.projection_selection_widget.resize(400, 30)
#         self.projection_selection_widget.setCrs(QgsCoordinateReferenceSystem('EPSG:4326'))
#         self.toolBar.addAction(self.mActionZoomTo)
#         self.toolBar.addAction(self.mActionPan)
#         self.toolBar.addAction(self.mActionFlash)
#         self.toolBar.addWidget(self.projection_selection_widget)

#         self.retranslateUi(ZoomToCoordinates)
#         QtCore.QMetaObject.connectSlotsByName(ZoomToCoordinates)

#     def retranslateUi(self, ZoomToCoordinates):
#         ZoomToCoordinates.setWindowTitle(QtGui.QApplication.translate("ZoomToCoordinates", "Zoom To Coordinates", None))
#         self.label.setText(QtGui.QApplication.translate("ZoomToCoordinates", "X:", None))
#         self.label_2.setText(QtGui.QApplication.translate("ZoomToCoordinates", "Y:", None))
#         self.toolBar.setWindowTitle(QtGui.QApplication.translate("ZoomToCoordinates", "toolBar", None))
#         self.mActionZoomTo.setText(QtGui.QApplication.translate("ZoomToCoordinates", "ZoomTo", None))
#         self.mActionZoomTo.setToolTip(QtGui.QApplication.translate("ZoomToCoordinates", "Zoom To Point", None))
#         self.mActionPan.setText(QtGui.QApplication.translate("ZoomToCoordinates", "Pan", None))
#         self.mActionPan.setToolTip(QtGui.QApplication.translate("ZoomToCoordinates", "Pan To", None))
#         self.mActionFlash.setText(QtGui.QApplication.translate("ZoomToCoordinates", "Flash", None))
#         self.mActionFlash.setToolTip(QtGui.QApplication.translate("ZoomToCoordinates", "Flash Point", None))

# #import resources_rc
